import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";

class WatchList extends Component {
  render() {
    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Fav</div>
          <div className="home__grid container">
            {this.props.favorits.length === 0
              ? null
              : this.props.favorits.map((movies, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/movie/${movies.id}`}
                      >
                        <img
                          src={`${process.env.REACT_APP_API_IMAGE_URL}/${movies.poster_path}`}
                          alt=""
                        />
                        <div>{movies.title}</div>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    favorits: state.movieDB.favoritMovies,
  };
};

export default connect(mapStateToProps)(WatchList);
