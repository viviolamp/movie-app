import React, { useEffect, useState } from "react";
import { Switch, NavLink, Route } from "react-router-dom";
import API from "API";

import Layout from "Components/Layout";
import DetailTabCharacter from "./DetailTab/DetailTabCharacter";
import DetailTabReview from "./DetailTab/DetailTabReview";
import DetailTabOverview from "./DetailTab/DetailTabOverview";

export default function Detail(props) {
  const [movie, setMovie] = useState(null);

  const fetchDetailMovie = () => {
    API.get(
      `/movie/${props.match.params.id}?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}&language=en-US`
    )
      .then((response) => setMovie(response.data))
      .catch((err) => console.log(err));
  };
  useEffect(fetchDetailMovie, []);

  return (
    <Layout>
      <div className="detail">
        <h1>Halaman detail: {movie?.title}</h1>
        <div className="detail__banner">
          {!movie ? null : (
            <img
              src={`${process.env.REACT_APP_API_IMAGE_URL}/${movie.poster_path}`}
              alt="image"
            />
          )}

          <h1 className="detail__banner__desc">Ini teks</h1>
        </div>
      </div>
      <div className="detail">
        <h1>Halaman Detail : {movie?.title}</h1>
        <NavLink exact to={`/movie/${props.match.params.id}`}>
          Review
        </NavLink>
        <NavLink exact to={`/movie/${props.match.params.id}/character`}>
          Character
        </NavLink>
        <NavLink exact to={`/movie/${props.match.params.id}/overview`}>
          Overview
        </NavLink>

        <Switch>
          <Route
            exact
            path="/movie/:id/character"
            component={DetailTabCharacter}
          />
          <Route
            exact
            path="/movie/:id/overview"
            component={DetailTabOverview}
          />
          <Route exact path="/movie/:id/" component={DetailTabReview} />
        </Switch>
      </div>
    </Layout>
  );
}
