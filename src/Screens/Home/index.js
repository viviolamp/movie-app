import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";
import { getMovies, setFavoritMovie } from "Store/Action/MovieDB";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      whichPage: 1,
    };
  }

  componentDidMount() {
    this.props.getMovies({ id: this.state.whichPage });
  }

  render() {
    const { offset } = this.state;
    const { movies, favorits } = this.props;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Watch List</div>
          <div className="home__grid container">
            {favorits?.length === 0 ? (
              <div>There is no watch list 😢</div>
            ) : (
              favorits?.map((movies, index) => {
                return (
                  <div className="home__grid__item" key={index}>
                    <Link
                      className="home__grid__item__content"
                      to={`/movie/${movies.id}`}
                    >
                      <img
                        src={`${process.env.REACT_APP_API_IMAGE_URL}/${movies.poster_path}`}
                        alt=""
                      />
                      <div>{movies.title}</div>
                    </Link>
                  </div>
                );
              })
            )}
          </div>

          <div className="home__title">Movie List</div>
          <div className="home__grid container">
            {movies.length === 0
              ? null
              : movies.map((movies, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={() =>
                          this.props.setFavoritMovie({
                            id: movies.id,
                            title: movies.title,
                            poster_path: movies.poster_path,
                          })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/movie/${movies.id}`}
                      >
                        <img
                          src={`${process.env.REACT_APP_API_IMAGE_URL}/${movies.poster_path}`}
                          alt=""
                        />
                        <div>{movies.title}</div>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

//map state to props adalag fungsi untuk me-mapping data dari store-redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
  return {
    favorits: state.movieDB.favoritMovies,
    movies: state.movieDB.movies,
  };
};

//export default connect(mapping-state-to-props, mapping-dispatch)(component-name)
export default connect(mapStateToProps, { getMovies, setFavoritMovie })(Home);
