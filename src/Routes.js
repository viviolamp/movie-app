import Home from "Screens/Home";
import WatchList from "Screens/WatchList";
import About from "Screens/About";
import Detail from "Screens/Detail";

export const publicRoutes = [
  {
    component: Home,
    path: "/",
    exact: true,
  },
  {
    component: About,
    path: "/about",
    exact: true,
  },
  {
    component: Detail,
    path: "/movie/:id",
  },
  {
    component: WatchList,
    path: "/watch-list",
    exact: true,
  },
];
