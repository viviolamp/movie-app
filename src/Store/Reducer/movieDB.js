const initalState = {
  favoritMovies: JSON.parse(localStorage.getItem("watchList")) ?? [],
  movies: [],
};

export default (state = initalState, action) => {
  switch (action.type) {
    case "SET_FAVORIT_POKEMON":
      return {
        ...state,
        favoritMovies: state.favoritMovies.concat(action.payload),
      };
    case "GET_POKEMON":
      return {
        ...state,
        movies: action.payload,
      };
    default:
      break;
  }
  return state;
};
