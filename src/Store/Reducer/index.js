import { combineReducers } from "redux";

import movieDB from "./movieDB";

export default combineReducers({ movieDB });
