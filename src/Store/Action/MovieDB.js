import API from "API";

export const getMovies = (params) => (dispatch) => {
  const { id } = params;
  API.get(
    `/list/${id}?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}&language=en-US`
  )
    .then((response) => {
      if (response.status === 200) {
        dispatch({
          type: "GET_POKEMON",
          payload: response.data.items,
        });
      }
    })
    .catch((err) => console.log(err));
};

export const setFavoritMovie = (params) => (dispatch) => {
  dispatch({
    type: "SET_FAVORIT_POKEMON",
    payload: params,
  });

  console.log(JSON.parse(localStorage.getItem("watchList")));

  !localStorage.getItem("watchList")
    ? localStorage.setItem("watchList", JSON.stringify([{ ...params }]))
    : localStorage.setItem(
        "watchList",
        JSON.stringify(
          JSON.parse(localStorage.getItem("watchList")).concat({ ...params })
        )
      );
};
