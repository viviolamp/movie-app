import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import "Styles/Main.scss";
import App from "./App";
import storeRedux from "./Store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={storeRedux}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
